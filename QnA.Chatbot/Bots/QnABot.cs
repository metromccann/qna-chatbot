using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Options;
using QnA.Chatbot.Configuration;
using QnA.Chatbot.Models;
using QnA.Chatbot.Services.Interfaces;

namespace QnA.Chatbot.Bots
{
    public class QnABot : ActivityHandler
    {
        private readonly IQnAService _qnaService;
        private readonly IOptions<AppSettings> _appSettings;

        public QnABot(IQnAService qnaService, IOptions<AppSettings> options)
        {
            _qnaService = qnaService;
            _appSettings = options;
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var answer = await GetAnswer(turnContext.Activity.Text);
            await turnContext.SendActivityAsync(MessageFactory.Text(answer), cancellationToken);
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            var welcomeText = $"Hello and welcome to the {_appSettings.Value.BotName}, What can I help you with?";
            foreach (var member in membersAdded)
            {
                if (member.Id != turnContext.Activity.Recipient.Id)
                {
                    await turnContext.SendActivityAsync(MessageFactory.Text(welcomeText, welcomeText), cancellationToken);
                }
            }
        }

        private async Task<string> GetAnswer(string text)
        {
            var request = new QnAServiceRequest()
            {
                Question = text
            };

            var response = await _qnaService.GetAnswers(request);

            if (string.IsNullOrEmpty(response.Answers.FirstOrDefault().Answer))
            {
                return "Sorry, I'm having trouble finding an answer for you. Try a different question";
            }

            return response.Answers.FirstOrDefault().Answer;
        }
    }
}
