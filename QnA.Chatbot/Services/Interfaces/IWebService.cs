﻿using System.Net;
using System.Threading.Tasks;
using QnA.Chatbot.Models;

namespace QnA.Chatbot.Services.Interfaces
{
    public interface IWebService
    {
        Task<QnAResponse> Post(string endpoint, QnAServiceRequest request, string auth);
    }
}
