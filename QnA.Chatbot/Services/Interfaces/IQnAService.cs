﻿using System.Threading.Tasks;
using QnA.Chatbot.Models;

namespace QnA.Chatbot.Services.Interfaces
{
    public interface IQnAService
    {
        Task<QnAResponse> GetAnswers(QnAServiceRequest request);
    }
}
