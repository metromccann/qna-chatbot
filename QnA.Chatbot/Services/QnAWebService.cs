﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using QnA.Chatbot.Models;
using QnA.Chatbot.Services.Interfaces;

namespace QnA.Chatbot.Services
{
    public class QnAWebService : IWebService
    {
        //private readonly ILogger _logger;

        public QnAWebService()
        {
            //_logger = logger;
        }

        public async Task<QnAResponse> Post(string endpoint, QnAServiceRequest requestData, string auth)
        {
            var qnaResponse = new QnAResponse();
            var result = string.Empty;

            try
            {
                using (var client = new HttpClient())
                using (var request = new HttpRequestMessage(HttpMethod.Post, endpoint))
                {
                    request.Headers.Add("Authorization", auth);
                    var json = JsonConvert.SerializeObject(requestData);
                    using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        request.Content = stringContent;

                        using (var response = await client.SendAsync(request, HttpCompletionOption.ResponseHeadersRead).ConfigureAwait(false))
                        {
                            result = await response.Content.ReadAsStringAsync();
                            qnaResponse.IsSuccess = response.StatusCode == HttpStatusCode.OK;
                        }
                    }
                }

                qnaResponse = JsonConvert.DeserializeObject<QnAResponse>(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Error when trying to post to endpoint {endpoint} ERROR: {ex.Message}");
                return new QnAResponse()
                {
                    IsSuccess = false,
                    Answers = new List<Answers>()
                };
            }

            return qnaResponse;
        }
    }
}
