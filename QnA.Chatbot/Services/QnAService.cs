﻿using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;
using QnA.Chatbot.Configuration;
using QnA.Chatbot.Models;
using QnA.Chatbot.Services.Interfaces;

namespace QnA.Chatbot.Services
{
    public class QnAService : IQnAService
    {
        private readonly IWebService _webService;
        private readonly IOptions<AppSettings> _appSettings;

        public QnAService(IWebService webService, IOptions<AppSettings> options)
        {
            _webService = webService;
            _appSettings = options;
        }
        
        public async Task<QnAResponse> GetAnswers(QnAServiceRequest request)
        {
            if (string.IsNullOrEmpty(request.Question))
                throw new ArgumentException("Question cannot be null or empty");

            var qnaResponse = await _webService.Post(_appSettings.Value.QnaServiceEndpoint,
                                                request,
                                                _appSettings.Value.QnaServiceEndpointKey);

            return qnaResponse;
        }
    }
}
