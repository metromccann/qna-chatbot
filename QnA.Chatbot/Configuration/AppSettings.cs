﻿namespace QnA.Chatbot.Configuration
{
    public class AppSettings
    {
        public string QnaServiceEndpoint { get; set; }
        public string QnaServiceEndpointKey { get; set; }
        public string BotName { get; set; }

        public AppSettings()
        {
            BotName = "BotName";
            QnaServiceEndpoint = "QnaServiceEndpoint";
            QnaServiceEndpointKey = "QnaServiceEndpointKey";
    }
    }
}
