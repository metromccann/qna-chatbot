﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace QnA.Chatbot.Models
{
    [Serializable]
    public class QnAResponse
    {
        public bool IsSuccess { get; set; }

        [JsonProperty(PropertyName = "answers")]
        public IEnumerable<Answers> Answers { get; set; }
    }

    [Serializable]
    public class Answers
    {
        [JsonProperty(PropertyName = "questions")]
        public IEnumerable<string> Questions { get; set; }

        [JsonProperty(PropertyName = "answer")]
        public string Answer { get; set; }

        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "score")]
        public double Score { get; set; }
    }
}
