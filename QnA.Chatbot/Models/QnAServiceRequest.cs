﻿using Newtonsoft.Json;

namespace QnA.Chatbot.Models
{
    public class QnAServiceRequest
    {
        [JsonProperty(PropertyName = "question")]
        public string Question { get; set; }
    }
}
